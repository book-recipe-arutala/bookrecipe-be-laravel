<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\RecipeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/book-recipes', [RecipeController::class, 'index']);
Route::get('/book-recipes/{recipeid}', [RecipeController::class, 'show']);
Route::post('/book-recipes', [RecipeController::class, 'store']);
Route::put('/book-recipes/{recipeid}', [RecipeController::class, 'update']);
Route::delete('/book-recipes/{recipeid}', [RecipeController::class, 'destroy']);

Route::get('/book-recipe-masters/level-option-lists', [LevelController::class, 'index']);
Route::get('/book-recipe-masters/category-option-lists', [CategoryController::class, 'index']);
