<?php

namespace App\Helpers;

class ResponseHelper
{
    protected static $status = [
        200 => 'OK',
        201 => 'Created',
        400 => 'Bad Request',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Server Error',
        422 => 'Unprocessable Content'
    ];

    /**
     * Membuat response API yang konsisten.
     *
     * @param int $statusCode Kode status HTTP.
     * @param string $message Pesan yang akan dikirim.
     * @param int|null $total Jumlah total item, jika relevan.
     * @param mixed $data Data yang akan dikirim.
     * @return array Response yang terstruktur.
     */
    public static function response(int $statusCode, string $message, ?int $total = 0, $data = null): array {
        $statusMessage = self::$status[$statusCode] ?? 'Unknown Status';

        $response = [
            'total' => $total,
            'message' => $message,
            'statusCode' => $statusCode,
            'status' => $statusMessage
        ];

        if ($data !== null) {
            $response['data'] = $data;
        }

        return $response;
    }
}
