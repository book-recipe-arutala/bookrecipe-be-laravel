<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    protected $table = 'recipes';
    protected $primaryKey = 'recipe_id';

    public $incrementing = false;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modified_time';

    protected $fillable = [
        'recipe_id',
        'category_id',
        'level_id',
        'recipe_name',
        'image_filename',
        'time_cook',
        'ingredient',
        'how_to_cook',
    ];


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id', 'level_id');
    }
}
