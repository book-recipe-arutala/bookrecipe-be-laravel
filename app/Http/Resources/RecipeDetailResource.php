<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipeDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'recipeId' => $this->recipe_id,
            // 'categories' => new CategoryResource($this->whenLoaded('category')),
            // 'levels' => new LevelResource($this->whenLoaded('level')),
            'categories' => [
                'categoryId' => $this->category->category_id,
                'categoryName' => $this->category->category_name
            ],
            'levels' => [
                'levelId' => $this->level->level_id,
                'levelName' => $this->level->level_name
            ],
            'recipeName' => $this->recipe_name,
            'imageFileName' => $this->image_filename,
            'timeCook' => $this->time_cook,
            'ingredient' => $this->ingredient,
            'howToCook' => $this->how_to_cook,
        ];
    }
}
