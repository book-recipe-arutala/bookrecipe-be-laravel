<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'recipeId' => $this->recipe_id,
            'categories' => [
                'categoryId' => $this->category->category_id,
                'categoryName' => $this->category->category_name
            ],
            'levels' => [
                'levelId' => $this->level->level_id,
                'levelName' => $this->level->level_name
            ],
            'recipeName' => $this->recipe_name,
            'imageFileName' => $this->image_filename,
            'timeCook' => $this->time_cook,
        ];
    }
}
