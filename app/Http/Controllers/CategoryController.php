<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Support\Facades\Log;
use Throwable;

class CategoryController extends Controller
{
    public function index()
    {
        try {
            $data = Category::all();
            if ($data->isEmpty()) {
                $resData = ResponseHelper::response(404, 'Data kategori masakan kosong', 0);
                return response()->json($resData, 404);
            }

            $resData = ResponseHelper::response(200, 'Berhasil mengambil data kategori', $data->count());
            return CategoryResource::collection($data)->additional($resData);
        } catch (Throwable $error) {
            Log::error($error->getMessage());
            $resData = ResponseHelper::response(500, 'Terjadi kesalahan server, silahkan coba kembali', 0);
            return response()->json($resData, 500);
        }
    }
}
