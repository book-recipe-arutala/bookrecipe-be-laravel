<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\RecipeDetailResource;
use App\Http\Resources\RecipeResource;
use App\Models\Category;
use App\Models\Level;
use App\Models\Recipe;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

use function PHPUnit\Framework\isNull;

class RecipeController extends Controller
{
    //
    public function index(Request $request)
    {
        // try {
            $query = Recipe::query();

            $this->applyFilters($query, $request);
            $this->applyTimeCookFilter($query, $request);
            $this->applySorting($query, $request);

            $pageSize = $request->input('pageSize', 8);
            $pageNumber = $request->input('pageNumber', 1);
            $recipesData = $query->paginate($pageSize, ['*'], 'page', $pageNumber);

            $counted = $recipesData->total();

            if ($counted === 0) {
                $resData = ResponseHelper::response(404, 'Resep masakan tidak tersedia', 0, []);
                return response()->json($resData, 404);
            }

            $resData = ResponseHelper::response(200, 'success', $counted);
            return RecipeResource::collection($recipesData)->additional($resData);
        // } catch (Exception $error) {
        //     Log::error($error);
        //     $resData = ResponseHelper::response(500, 'Terjadi kesalahan server, silahkan coba kembali.');
        //     return response()->json($resData, 500);
        // }
    }

    public function show($id)
    {
        try {
            $recipe = Recipe::find($id);

            if ($recipe === null) {
                $resData = responseHelper::response(404, 'Detil Resep masakan tidak tersedia', 0);
                return response()->json($resData, 404);
            }

            $resData = responseHelper::response(200, 'Berhasil memuat Resep Masakan "' . $recipe->recipe_name . '"', 1);
            return (new RecipeDetailResource($recipe))->additional($resData);
        } catch (Throwable $error) {
            Log::info($error->getMessage());
            $resData = responseHelper::response(500, 'Terjadi kesalahan server. Silahkan coba kembali', 0);
            return response()->json($resData, 500);
        }
    }

    public function store(Request $request)
    {
        // Validasi data request yang masuk
        $validatedData = $request->validate([
            'recipeName' => 'required|string',
            'imageFilename' => 'required|url',
            'timeCook' => 'required|integer|min:0',
            'ingredient' => 'required|string',
            'howToCook' => 'required|string',
        ]);

        // Cek apakah kategori dan level ada
        $categoryId = $request->input('categories.categoryId');
        $levelId = $request->input('levels.levelId');

        if (!Category::where('category_id', $categoryId)->exists()) {
            return response()->json(['message' => 'Kategori tidak ditemukan'], 404);
        }

        if (!Level::where('level_id', $levelId)->exists()) {
            return response()->json(['message' => 'Level tidak ditemukan'], 404);
        }

        // Membuat instance Recipe baru dan mengisinya dengan data yang divalidasi
        $recipe = Recipe::create([
            'recipe_id' => DB::select("SELECT nextval('recipes_recipe_id_seq')")[0]->nextval,
            'category_id' => $categoryId,
            'level_id' => $levelId,
            'recipe_name' => $validatedData['recipeName'],
            'image_filename' => $validatedData['imageFilename'],
            'time_cook' => $validatedData['timeCook'],
            'ingredient' => $validatedData['ingredient'],
            'how_to_cook' => $validatedData['howToCook'],
        ]);

        // Mengembalikan respons

        $resData = responseHelper::response(200, 'Resep "' . $recipe->recipe_name . '" berhasil dibuat', 1);
        return $resData;
    }

    public function update(Request $request, $recipeid)
    {
        try {
            $validatedData = $request->validate([
                'recipeName' => 'required|string',
                'imageFilename' => 'required|url',
                'timeCook' => 'required|integer|min:0',
                'ingredient' => 'required|string',
                'howToCook' => 'required|string',
            ]);

            $recipe = Recipe::find($recipeid);
            if (!$recipe) {
                $resData = responseHelper::response(404, 'Resep tidak ditemukan', 0);
                return response()->json($resData, 404);
            }

            $categoryId = $request->input('categories.categoryId');
            $levelId = $request->input('levels.levelId');

            if (!Category::where('category_id', $categoryId)->exists()) {
                return responseHelper::response(404, 'Kategori tidak ditemukan');
            }

            if (!Level::where('level_id', $levelId)->exists()) {
                return responseHelper::response(404, 'Level tidak ditemukan');
            }

            $recipe->category_id = $categoryId;
            $recipe->level_id = $levelId;
            $recipe->recipe_name = $validatedData['recipeName'];
            $recipe->image_filename = $validatedData['imageFilename'];
            $recipe->time_cook = $validatedData['timeCook'];
            $recipe->ingredient = $validatedData['ingredient'];
            $recipe->how_to_cook = $validatedData['howToCook'];

            $recipe->save();

            $resData = responseHelper::response(200, 'Resep "' . $recipe->recipe_name . '" berhasil diubah!', 1);
            return $resData;
        } catch (Throwable $error) {
            Log::info($error->getMessage());
            $resData = responseHelper::response(500, 'Terjadi kesalahan server. Silahkan coba kembali', 0);
            return response()->json($resData, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $recipe = Recipe::find($id);

            if (!$recipe) {
                $resData = responseHelper::response(404, 'Resep tidak ditemukan');
                return response()->json($resData, 404);
            }

            $recipe->delete();

            $resData = responseHelper::response(200, 'Resep "' . $recipe->recipe_name . '" berhasil dihapus!', 1);

            return $resData;
        } catch (Throwable $error) {
            Log::info($error->getMessage());
            $resData = responseHelper::response(500, 'Terjadi kesalahan server. Silahkan coba kembali', 0);
            return response()->json($resData, 500);
        }
    }

    private function applyFilters(&$query, $request)
    {
        if ($request->recipeName) {
            $query->where('recipe_name', 'ILIKE', '%' . strtolower($request->recipeName) . '%');
        }

        if ($request->has('categoryId')) {
            $query->where('category_id', $request->input('categoryId'));
        }

        if ($request->has('levelId')) {
            $query->where('level_id', $request->input('levelId'));
        }
    }

    private function applyTimeCookFilter(&$query, $request)
    {
        if ($request->timeCook) {
            $timeCook = $request->timeCook;
            if ($timeCook == '0-30') {
                $query->where('time_cook', '<=', 30);
            }elseif ($timeCook == '30-60') {
                $query->whereBetween('time_cook', [31, 60]);
            }elseif ($timeCook == '60') {
                $query->where('time_cook', '>', 60);
            }
        }
    }

    private function applySorting(&$query, $request)
    {
        if ($request->filled('sortBy')) {
            $sortByParts = explode(',', $request->input('sortBy'));
            list($sortBy, $direction) = $sortByParts;
            $direction = strtolower($direction) === 'desc' ? 'desc' : 'asc';

            if ($sortBy === 'recipeName') {
                $query->orderBy('recipe_name', $direction);
            } elseif ($sortBy === 'timeCook') {
                $query->orderBy('time_cook', $direction);
            }
        } else {
            $query->orderBy('recipe_name', 'asc');
        }
    }
}
