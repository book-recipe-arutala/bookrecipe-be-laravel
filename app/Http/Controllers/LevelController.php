<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\LevelResource;
use App\Models\Level;
use Illuminate\Support\Facades\Log;

class LevelController extends Controller
{
    public function index()
    {
        try {
            $levels = Level::all();
            $resData = ResponseHelper::response(200, 'Level berhasil dimuat', $levels->count());
            return LevelResource::collection($levels)->additional($resData);
        } catch (\Throwable $error) {
            Log::error($error->getMessage());
            $resData = ResponseHelper::response(500, 'Terjadi kesalahan server, silahkan coba kembali.', 0);
            return response()->json($resData, 500);
        }
    }
}
